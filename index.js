var express = require('express'),
 app = express(),
 morgan  = require('morgan');

//var http = require('http'),
	//server = http.Server(app);
	
	app.engine('html', require('ejs').renderFile);
	app.use(morgan('combined'))

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

//app.use(express.static('client'));

app.listen(port, ip, function() {
  console.log('Server running on http://%s:%s', ip, port);
});
module.exports = app ;

var io = require('socket.io')(server);

io.on('connection', function(socket) {
  socket.on('message', function(msg) {
    io.emit('message', msg);
  });
});